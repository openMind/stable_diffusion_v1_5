import os
import argparse

import torch
from diffusers import StableDiffusionPipeline
from openmind import is_torch_npu_available
from openmind_hub import snapshot_download


def parse_args():
    parser = argparse.ArgumentParser(description="Eval the model")
    parser.add_argument(
        "--model_name_or_path",
        type=str,
        help="Path to the model",
        default=None,
        )
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    if args.model_name_or_path:
        model_path = args.model_name_or_path
    else:
        model_path = snapshot_download("PyTorch-NPU/stable_diffusion_v1_5", revision="main", resume_download=True,
                                       ignore_patterns=["*.h5", "*.ot", "*.msgpack"])
    if is_torch_npu_available():
        device = "npu:0"
    else:
        device = "cpu"

    pipe = StableDiffusionPipeline.from_pretrained(model_path, torch_dtype=torch.float16)
    pipe = pipe.to(device)
    generator = torch.Generator(device=device).manual_seed(1234)

    prompt = "a photo of an astronaut riding a horse on mars"
    image = pipe(prompt, generator=generator).images[0]

    image.save("astronaut_rides_horse.png")


if __name__ == '__main__':
    main()
